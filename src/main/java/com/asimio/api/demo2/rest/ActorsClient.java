package com.asimio.api.demo2.rest;

import java.util.List;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.asimio.api.demo2.model.Actor;

@FeignClient(name = ActorsClient.ACTORS_SERVIDE_ID)
public interface ActorsClient {

	String ACTORS_SERVIDE_ID = "the-demo-registration-api-1";
	String ACTORS_ENDPOINT = "/actors";
	String ACTOR_BY_ID_ENDPOINT = "/actors/{id}";

	@RequestMapping(method = RequestMethod.GET, value = ACTORS_ENDPOINT)
	List<Actor> findActors();

	@RequestMapping(method = RequestMethod.GET, value = ACTOR_BY_ID_ENDPOINT)
	Actor getActor(@PathVariable("id") String id);
}