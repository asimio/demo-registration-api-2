# README #

Demo Registration and Discovery 2 Service's accompanying source code for blog entry at http://tech.asimio.net/2016/11/14/Microservices-Registration-and-Discovery-using-Spring-Cloud-Eureka-Ribbon-and-Feign.html

### Requirements ###

* Java 8
* Maven 3.3.x
* Docker host or Docker machine
* Running Eureka cluster

### Building and executing the application from command line ###

```
mvn clean package
java -DappPort=8601 -DhostName=$HOSTNAME -Deureka.client.serviceUrl.defaultZone="http://$HOSTNAME:8001/eureka/,http://$HOSTNAME:8002/eureka/" -jar target/demo-registration-api-2.jar
```

Eureka cluster Open http://localhost:8001 or http://localhost:8002 in a browser

### How do I get set up using Docker? ###

```
sudo docker pull asimio/demo-registration-api-2

Multiple containers:
sudo docker run -idt -p 8601:8601 --net=host -e appPort=8601 -e hostName=$HOSTNAME -e eureka.client.serviceUrl.defaultZone="http://$HOSTNAME:8001/eureka/,http://$HOSTNAME:8002/eureka/" asimio/demo-registration-api-2:1.0.20
sudo docker run -idt -p 8602:8602 --net=host -e appPort=8602 -e hostName=$HOSTNAME -e eureka.client.serviceUrl.defaultZone="http://$HOSTNAME:8001/eureka/,http://$HOSTNAME:8002/eureka/" asimio/demo-registration-api-2:1.0.20
```

Open http://localhost:8001 or http://localhost:8002 in a browser

### Who do I talk to? ###

* ootero at asimio dot net
* https://www.linkedin.com/in/ootero